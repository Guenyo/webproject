from django.db import models
from django.forms import ModelForm, TextInput
# import countries um Flaggen anzuzeigen
from django_countries.fields import CountryField
from django_countries import Countries
from django_countries.widgets import CountrySelectWidget

# Create your models here.
# Klasse, die die Länder Kürzel für das modul django-countries beinhaltet
# Es sollte nur die Länder benutzt werden, für die ich auch eine Flagge habe.
class All_Countries(Countries):
    only = [
        'AD', 'AF', 'AI', 'AL', 'AM', 'AO', 'AR', 'AS', 'AT', 'AU', 'AW', 'AZ', 'BB', 
		'BD', 'BE', 'BG', 'BH', 'BI', 'BJ', 'BM', 'BN', 'BO', 'BQ', 'BR', 'BS', 'BT', 
		'BW', 'BY', 'BZ', 'CA', 'CH', 'CL', 'CM', 'CN', 'CO', 'CU', 'CW', 'CY', 'DE', 
		'DJ', 'DK', 'DM', 'DZ', 'EC', 'EE', 'EG', 'ER', 'ES', 'ET', 'FI', 'FJ', 'FM', 
		'FR', 'GA', 'GB', 'GD', 'GE', 'GG', 'GH', 'GI', 'GL', 'GM', 'GQ', 'GR', 'GT', 
		'GU', 'GW', 'HN', 'HR', 'HT', 'HU', 'ID', 'IE', 'IL', 'IO', 'IQ', 'IR', 'IS', 
		'IT', 'JE', 'JM', 'JO', 'JP', 'KE', 'KG', 'KH', 'KM', 'KW', 'KZ', 'LA', 'LB', 
		'LI', 'LR', 'LS', 'LT', 'LU', 'LV', 'LY', 'MA', 'MC', 'MD', 'ME', 'MG', 'ML', 
		'MM', 'MN', 'MO', 'MQ', 'MR', 'MS', 'MT', 'MU', 'MV', 'MW', 'MX', 'MY', 'MZ', 
		'NA', 'NE', 'NG', 'NI', 'NL', 'NO', 'NP', 'NR', 'NU', 'OM', 'PA', 'PE', 'PH', 
		'PK', 'PL', 'PS', 'PT', 'PW', 'PY', 'QA', 'RO', 'RS', 'RU', 'RW', 'SC', 'SE', 
		'SG', 'SI', 'SK', 'SN', 'SO', 'SR', 'SS', 'SY', 'TD', 'TG', 'TH', 'TJ', 'TK', 
		'TM', 'TN', 'TO', 'TR', 'TW', 'TZ', 'UA', 'UG', 'UY', 'VE', 'VN', 'VU', 'YE', 
		'ZM', 'ZW'
    ]

# Ein Highscore/Player Eintrag
class Highscore(models.Model):
	name = models.TextField()
	score = models.IntegerField(default=0)
	country = CountryField(countries = All_Countries,blank_label='select country',countries_flag_url='yilmaz/flags/{code}.svg') 

	def _str__(self):
		return "{}".format(self.name)

class HighscoreForm(ModelForm):
	class Meta:
		model = Highscore
		fields = ["name","country"]

