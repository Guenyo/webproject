import os
import json
import requests

# counter is only for tracking purpose, want to see how many file i went through
counter = 0

# go through directory (string is path)
for filename in os.listdir("C:/Users/Güney/Downloads/555416-rectangular-country-simple-flags/555416-rectangular-country-simple-flags/svg"):
        # only want to change SVG's
        if filename.endswith(".svg"):
          # try/except because not every country in the api
           try:
                # json api (REST Country) to get country code, complete the url path with the filename except the file ending (.svg)
                url = "https://restcountries.eu/rest/v2/name/{}".format(filename[:-4])
                response = requests.get(url) 
                data = json.loads(response.content)
                # if request failed it will only send a dict instead of a list, dicts behave differently
                if type(data) == dict:
                    # if status 404 go on to the next file
                    if data["status"] == 404: 
                        counter += 1
                        continue
                else:
                    new_filename = data[0]["alpha2Code"].lower()

                    new_filename = new_filename + ".svg"
                    print(new_filename)
                    src = "C:/Users/Güney/Downloads/555416-rectangular-country-simple-flags/555416-rectangular-country-simple-flags/svg/{}".format(filename)
                #print(src)
                    os.rename(filename, new_filename)
                    counter += 1
           except:
               continue
               
        else:
            continue
print("counter", counter)