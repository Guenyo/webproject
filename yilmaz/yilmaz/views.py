from django.shortcuts import render, redirect, reverse, get_object_or_404
from django.urls import reverse
from django.http import HttpResponse
from random import randint
from . import models
from .models import HighscoreForm #ScoreForm
from django.views.decorators.cache import never_cache


leben = 5 
punkte = 0
context = {}
#Startseite mit Highscore Liste
def startpage(request):
    # context = die letzten 3 Einträge sortiert nach den Punkten(Score)
    count = models.Highscore.objects.all().count() 
    context = models.Highscore.objects.order_by("-score")[:3]
    return render(request,"yilmaz/start.html",{"context":context, "count":count})



# spieler zumindest versuchen daran zu hindern, dass er durch das zurück gehen im Browser mehrere Spieler gleichzeitig anlegt.
@never_cache
# spieler erstellen
def create_player(request):
    # Form laden
    form = HighscoreForm()
    if request.POST:
        form = HighscoreForm(request.POST)
        if form.is_valid():
            player_name = form["name"].value()
            player_country = form["country"].value()
            # schauen ob Name schon in DB
            try:
                player = models.Highscore.objects.get(name = player_name)
                context = {"form":form,"error":"Der Name wurde schon benutzt"}
                return render(request,"yilmaz/create_player.html", context)
            # Wenn Fehler, dann ist der Name noch nicht in der DB, Spieler anlegen/speichern
            except:
                player = models.Highscore(name=player_name, country=player_country)
                player.save() 
                form = HighscoreForm()

                # Spielernamen über die URL weiter geben
                return redirect("game_view", player_name=player_name)
    return render(request,"yilmaz/create_player.html", {"form":form})
    



#Wenn Spiel vorbei Game Over Seite anzeigen mit Highscore Liste (Top 10)
def game_end(request):
    context = models.Highscore.objects.order_by("-score")[:10]
    return render(request,"yilmaz/game_end.html",{"context":context})

#Findet das Spiel statt
def game_view(request, player_name):
    global leben
    global punkte
    global context
    #Funktion um Leben abzuziehen und prüfen, ob Spieler verloren hat
    def lose_life():
        global leben
        global punkte
        leben -= 1
            # leben auf 0, Spiel wird neu gestartet
        if leben == 0:

            highscore = get_object_or_404(models.Highscore, name=player_name)
            print("higscore",highscore) 
            highscore.score = int(punkte)
            highscore.save()
            leben = 5
            punkte = 0 
            return True

    player_name = player_name
    # Infos die schon zu Beginn des Spieles angezeigt werden sollen
    context["Leben"] = leben
    context["Punkte"] = int(punkte)

    if "wahl_abschicken" in request.POST:
        
        #computer stimme wird erstellt & Spieler Stimme wird gespeichert
        computer = randint(0,2)
        player = int(request.POST.get("answer"))
        # Spielsituation wird duchgegangen
        if computer == 0:
            context["Computer"] = "Schere"
            if player == 2:
                context["Wahl"] = "Papier"
                context["Ergebnis"] = "Computer gewinnt"
                lost = lose_life()
                if lost:
                    return redirect("game_end")
            elif player == 1:
                context["Ergebnis"] = "Player gewinnt"
                context["Wahl"] = "Stein"
                punkte += 1
            else:
                context["Ergebnis"] = "Pat"
                context["Wahl"] = "Schere"


        elif computer == 1:
            context["Computer"] = "Stein"
            if player == 0:
                context["Wahl"] = "Schere"
                context["Ergebnis"] = "Computer gewinnt"
                lost = lose_life()
                if lost:
                    return redirect("game_end")
            elif player == 2:
                context["Ergebnis"] = "Player gewinnt"
                context["Wahl"] = "Papier"
                punkte += 1
            else:
                context["Ergebnis"] = "Pat"
                context["Wahl"] = "Stein"

        else:
            context["Computer"] = "Papier"
            if player == 0:
                context["Ergebnis"] = "Player gewinnt"
                context["Wahl"] = "Schere"
                punkte += 1
            elif player == 1:
                context["Wahl"] = "Stein"
                context["Ergebnis"] = "Computer gewinnt"
                lost = lose_life()
                if lost:
                    return redirect("game_end")
            else:
                context["Ergebnis"] = "Pat"
                context["Wahl"] = "Papier"
        
        context["Leben"] = leben
        return redirect("game_view", player_name= player_name)
    return render(request, "yilmaz/game.html",{"context":context})

    
    


